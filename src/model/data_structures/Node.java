package model.data_structures;

public class Node<E> {
private Node<E> next;
private E item;
public Node(E item) {
	this.item = item;
	next=null;
}
public Node<E> getNext() {
	return next;
}
public void setNext(Node<E> item) {
	this.next = item;
}
public E getItem() {
	return item;
}
public void setItem(E item) {
	this.item = item;
}


}
