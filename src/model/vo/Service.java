package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String company;
	private String dropoff_census_tract;
	private String dropoff_centroid_latitude;
	private String dropoff_centroid_location;
	private String dropoff_centroid_longitude;
	private String dropoff_community_area;
	private String extras;
	private String fare;
	private String payment_type;
	private String pickup_census_tract;
	private String pickup_centroid_latitude;
	private String pickup_centroid_location;
	private String pickup_centroid_longitude;
	private String pickup_community_area;
	private int taxi_id;
	private String tips;
	private String tolls;
	private String trip_end_timestamp;
	private int trip_id;
	private String trip_miles;
	private String trip_seconds;
	private String trip_start_timestamp;
	private String trip_total;
    public Service(String[] ser){
		company=ser[0];
		dropoff_census_tract=ser[1];
		dropoff_centroid_latitude=ser[2];
		dropoff_centroid_location=ser[3];
		dropoff_centroid_longitude=ser[4];
		dropoff_community_area=ser[5];
		extras=ser[6];
		fare=ser[7];
		payment_type=ser[8];
		pickup_census_tract=ser[9];
		pickup_centroid_latitude=ser[10];
		pickup_centroid_location=ser[11];
		pickup_centroid_longitude=ser[12];
		pickup_community_area=ser[13];
		taxi_id=Integer.parseInt(ser[14]);
		tips=ser[15];
		tolls=ser[16];
		trip_end_timestamp=ser[17];
		trip_id=Integer.parseInt(ser[18]);
		trip_miles=ser[19];
		trip_seconds=ser[20];
		trip_start_timestamp=ser[18];
		trip_total=ser[19];		
	}
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDropoff_census_tract() {
		return dropoff_census_tract;
	}
	public void setDropoff_census_tract(String dropoff_census_tract) {
		this.dropoff_census_tract = dropoff_census_tract;
	}
	public String getDropoff_centroid_latitude() {
		return dropoff_centroid_latitude;
	}
	public void setDropoff_centroid_latitude(String dropoff_centroid_latitude) {
		this.dropoff_centroid_latitude = dropoff_centroid_latitude;
	}
	public String getDropoff_centroid_location() {
		return dropoff_centroid_location;
	}
	public void setDropoff_centroid_location(String dropoff_centroid_location) {
		this.dropoff_centroid_location = dropoff_centroid_location;
	}
	public String getDropoff_centroid_longitude() {
		return dropoff_centroid_longitude;
	}
	public void setDropoff_centroid_longitude(String dropoff_centroid_longitude) {
		this.dropoff_centroid_longitude = dropoff_centroid_longitude;
	}
	public String getDropoff_community_area() {
		return dropoff_community_area;
	}
	public void setDropoff_community_area(String dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}
	public String getExtras() {
		return extras;
	}
	public void setExtras(String extras) {
		this.extras = extras;
	}
	public String getFare() {
		return fare;
	}
	public void setFare(String fare) {
		this.fare = fare;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	public String getPickup_census_tract() {
		return pickup_census_tract;
	}
	public void setPickup_census_tract(String pickup_census_tract) {
		this.pickup_census_tract = pickup_census_tract;
	}
	public String getPickup_centroid_latitude() {
		return pickup_centroid_latitude;
	}
	public void setPickup_centroid_latitude(String pickup_centroid_latitude) {
		this.pickup_centroid_latitude = pickup_centroid_latitude;
	}
	public String getPickup_centroid_location() {
		return pickup_centroid_location;
	}
	public void setPickup_centroid_location(String pickup_centroid_location) {
		this.pickup_centroid_location = pickup_centroid_location;
	}
	public String getPickup_centroid_longitude() {
		return pickup_centroid_longitude;
	}
	public void setPickup_centroid_longitude(String pickup_centroid_longitude) {
		this.pickup_centroid_longitude = pickup_centroid_longitude;
	}
	public String getPickup_community_area() {
		return pickup_community_area;
	}
	public void setPickup_community_area(String pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}
	public int getTaxi_id() {
		return taxi_id;
	}
	public void setTaxi_id(int taxi_id) {
		this.taxi_id = taxi_id;
	}
	public String getTips() {
		return tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}
	public String getTolls() {
		return tolls;
	}
	public void setTolls(String tolls) {
		this.tolls = tolls;
	}
	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}
	public void setTrip_end_timestamp(String trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}
	public int getTrip_id() {
		return trip_id;
	}
	public void setTrip_id(int trip_id) {
		this.trip_id = trip_id;
	}
	public String getTrip_miles() {
		return trip_miles;
	}
	public void setTrip_miles(String trip_miles) {
		this.trip_miles = trip_miles;
	}
	public String getTrip_seconds() {
		return trip_seconds;
	}
	public void setTrip_seconds(String trip_seconds) {
		this.trip_seconds = trip_seconds;
	}
	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}
	public void setTrip_start_timestamp(String trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}
	public String getTrip_total() {
		return trip_total;
	}
	public void setTrip_total(String trip_total) {
		this.trip_total = trip_total;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int compareTo(Service o) {
		if(taxi_id<o.getTaxi_id()){
			return -1;
		}
		else if(taxi_id> o.getTaxi_id()){
			return 1;
		}
		else{
		     if(company.compareTo(o.getCompany())<0){
		    	 return -1;
		     }
		     else if(company.compareTo(o.getCompany())>0){
		    	 return 1;
		     }
		     else{
		    	 return 0;
		     }
		}
	}
}
